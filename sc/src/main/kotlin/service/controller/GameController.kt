package service.controller

import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.Produces
import service.model.GameBuilder
import service.model.GameCard

@Controller("/secretcode/game")
class GameController {
    @Get("/")
    @Produces(MediaType.TEXT_PLAIN)
    fun index(): String {
        return "Total Games: " + GameBuilder.games.count()
    }

    @Post("/")
    @Produces(MediaType.APPLICATION_JSON)
    fun createGame():Pair<String, List<Triple<Int, String, String>>> {
        val c = GameBuilder.buildGame()
        return Pair(c.first,c.second.board())
    }


    @Get("/{gameId}")
    @Produces(MediaType.APPLICATION_JSON)
    fun getBoard(gameId: String): List<Triple<Int, String, String>> {
        return GameBuilder.getGame(gameId).board()
    }

    @Get("/{gameId}/secret")
    @Produces(MediaType.APPLICATION_JSON)
    fun getSecretBoard(gameId: String): Map<Int, GameCard> {
        return GameBuilder.getGame(gameId).secretBoard()
    }

    @Post("/{gameId}/showCard/{cardId}")
    @Produces(MediaType.APPLICATION_JSON)
    fun showCard(gameId: String, cardId: Int): List<Triple<Int, String, String>> {
        val game = GameBuilder.getGame(gameId)
        game.showCard(cardId)
        return game.board()
    }

    private fun findColor(c: String): String {
        return if (c == "RED") "#F1340C"
        else if (c == "BLUE") "#A4B6F5"
        else if (c == "YELLOW") "#F1F107"
        else if (c == "BLACK") "#727581"
        else "#FFFFFF"
    }

    @Get("html/{gameId}")
    @Produces(MediaType.TEXT_HTML)
    fun getHtmlBoard(gameId: String): String {
        val board = GameBuilder.getGame(gameId).board()

        fun th(it: Int):String {
            val c = board.get(it)
            val color = findColor(c.third)
            return "<th  style=\"background-color: " + color + "\">" +c.first+". " + c.second + "</th>"
        }

        val firstRow = (0..4).map {
            th(it)
        }
        val secondRow = (5..9).map {
            th(it)
        }

        val thirdRow = (10..14).map {
            th(it)
        }

        val fourthRow = (15..19).map {
            th(it)
        }

        val fifthRow = (20..24).map {
            th(it)
        }

        return """<html>
            |<head>
                <meta charset="UTF-8">
                </head>
                <table style="width:100%">""".trimMargin() +
                firstRow.joinToString(separator = "", prefix = "<tr>", postfix = "</tr>") +
                secondRow.joinToString(separator = "", prefix = "<tr>", postfix = "</tr>") +
                thirdRow.joinToString(separator = "", prefix = "<tr>", postfix = "</tr>") +
                fourthRow.joinToString(separator = "", prefix = "<tr>", postfix = "</tr>") +
                fifthRow.joinToString(separator = "", prefix = "<tr>", postfix = "</tr>") + "</table></html>"

    }

    @Get("html/{gameId}/secret")
    @Produces(MediaType.TEXT_HTML)
    fun getHtmlSecretBoard(gameId: String): String {

        val board = GameBuilder.getGame(gameId).secretBoard()

        fun th(it:Int):String {
            val c = board.get(it)!!
            val color = findColor(c.card.color.toString())
            val word = if (c.hidden) "" + it + ". " + c.card.word
            else ""

            return "<th  style=\"background-color: " + color + "\">" + word + "</th>"
        }


        val firstRow = (0..4).map {
            th(it)
        }
        val secondRow = (5..9).map {
            th(it)
        }

        val thirdRow = (10..14).map {
            th(it)
        }

        val fourthRow = (15..19).map {
            th(it)
        }

        val fifthRow = (20..24).map {
            th(it)
        }

        return """<html>
            |<head>
                <meta charset="UTF-8">
                </head>
                <table style="width:100%">""".trimMargin() +
                firstRow.joinToString(separator = "", prefix = "<tr>", postfix = "</tr>") +
                secondRow.joinToString(separator = "", prefix = "<tr>", postfix = "</tr>") +
                thirdRow.joinToString(separator = "", prefix = "<tr>", postfix = "</tr>") +
                fourthRow.joinToString(separator = "", prefix = "<tr>", postfix = "</tr>") +
                fifthRow.joinToString(separator = "", prefix = "<tr>", postfix = "</tr>") + "</table></html>"

    }


}