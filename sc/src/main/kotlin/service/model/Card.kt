package service.model

import java.util.*
import kotlin.random.Random


object GameBuilder {

    val games = mutableMapOf<String, Game>()

    fun buildGame():Pair<String, Game>  {
        val words = Dictionary.wordsForGame()
        val mutableSet = words.toMutableSet()
        val board = mutableMapOf<Int, GameCard>()

        (0..7).map {
            buildCard(words, mutableSet, Color.RED)
        }.map { p ->
            addGameCardsToBoar(board, p())
        }
        (0..6).map {
            buildCard(words, mutableSet, Color.BLUE)
        }.map { p ->
            addGameCardsToBoar(board, p())
        }

        (0..0).map {
            buildCard(words, mutableSet, Color.BLACK)
        }.map { p ->
            addGameCardsToBoar(board, p())
        }

        mutableSet.map { word ->
            Pair (words.indexOf(word), Card(word, Color.YELLOW))
        }.map { p ->
            addGameCardsToBoar(board, p)
        }

        val gameId = UUID.randomUUID().toString().substring(0,6)
        val game = Game(board)
        games.put(gameId, game)

        return Pair (gameId, game)

    }

    fun addGameCardsToBoar(board: MutableMap<Int, GameCard>, pair: Pair<Int, Card>) {
        board.put(pair.first, GameCard(pair.second))
    }


    fun buildCard(words: Set<String>, mutableWords: MutableSet<String>, color: Color) = {
        val i = Random.nextInt(0, mutableWords.size - 1)
        val word = mutableWords.elementAt(i)
        mutableWords.remove(word)
        Pair (words.indexOf(word), Card(word, color))
    }


    fun getGame(id: String): Game {
        return games.get(id)!!
    }

}




class Card (val word: String, val color: Color) {
    override fun toString(): String = "Card(" + word + "," + color + ")"
}

enum class Color {
    RED,
    BLUE,
    BLACK,
    YELLOW
}

class GameCard(val card: Card, var hidden: Boolean = true) {
    override fun toString(): String = card.toString() + ", hidden:" + hidden
}

class Game(val board: Map<Int, GameCard>) {

    fun showCard(order:Int) {
        this.board.get(order)!!.hidden = false
    }

    fun board(): List<Triple<Int, String, String>> {
        return this.board.map {
            c ->
            val color = if (c.value.hidden) {
                ""
            } else {
                c.value.card.color.toString()
            }

            Triple(c.key,c.value.card.word, color)
       }.sortedBy { c -> c.first }
    }

    fun secretBoard():Map<Int, GameCard> {
        return this.board.toSortedMap()
    }

}