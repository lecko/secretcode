package service.model
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import kotlin.test.assertEquals
import kotlin.test.assertTrue

object ModelSpec : Spek({
    describe("Check Dictionary") {

        it("Words must be 25") {
            val a = Dictionary.wordsForGame()()
            a.map { c -> println(c) }
            assertEquals(25, a.size)
        }

        it("game builder") {
            val game = GameBuilder.buildGame()


            println(game.first)
            println(game.second.secretBoard())
            println(game.second.board())

            val cachedGame = GameBuilder.getGame(game.first)

            assertEquals<Game>(game.second, cachedGame)
        }

        it("show 6 card") {
            val game = GameBuilder.buildGame()


            println(game.first)
            println(game.second.secretBoard())
            println(game.second.board())

            val card1 = game.second.board().get(6)
            assertTrue { card1.third == "" }


            game.second.showCard(6)

            println(game.second.board())
            val card = game.second.board().get(6)
            assertTrue { card.third != "" }
            println(card1)
            println(card)

        }

    }
})