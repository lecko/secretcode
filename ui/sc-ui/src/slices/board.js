import { createSlice } from '@reduxjs/toolkit'
import {buildWsUri, initWebSocket, emit} from "../websocket/websocket";

export const initialState = {
    loading: false,
    hasErrors: false,
    board: [],
    gameId: "",
    connected: ""
}

const boardSlice = createSlice({
    name: 'board',
    initialState,
    reducers: {
        getBoard :(state, { payload } ) => {
            state.loading = true
            state.gameId = payload
        },
        getBoardSuccess: (state,  { payload } ) => {
            state.board = payload
            state.loading = false
            state.hasErrors = false
        },
        getBoardFailure: state => {
            state.loading = false
            state.hasErrors = true
        },
        connected :(state, { payload } ) => {
            state.connected = payload
        },
    },
})

export const {
    getBoard,
    getBoardSuccess,
    getBoardFailure,
    connected
} = boardSlice.actions
export const boardSelector = state => state.board
export default boardSlice.reducer

export function fetchBoard(gameId) {
    return async dispatch => {

        dispatch(getBoard(gameId))

        try {
            const response = await fetch(
                `/secretcode/services/secretcode/game/${gameId}`
            )
            const data = await response.json()
            dispatch(getBoardSuccess(data))

        } catch (error) {
            console.log(error)
            dispatch(getBoardFailure())
        }
        if (boardSelector.connected !== gameId) {
            try {
                const uri = buildWsUri(gameId, "test")
                initWebSocket(dispatch, uri)
                dispatch(connected(gameId))
            } catch (error) {
                console.log(error)
            }
        }
    }
}

export function fetchBoardWs(gameId) {
    return async dispatch => {

        try {
            const response = await fetch(
                `/secretcode/services/secretcode/game/${gameId}`
            )
            const data = await response.json()
            dispatch(getBoardSuccess(data))

        } catch (error) {
            console.log(error)
            //dispatch(getBoardFailure())
        }
    }
}


export function showCard(gameId, cardId) {
    return async dispatch => {
        // dispatch(getBoard(gameId))

        try {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' }
            };
            const response = await fetch(
                `/secretcode/services/secretcode/game/${gameId}/showCard/${cardId}`,
                requestOptions
            )
            const data = await response.json()
            dispatch(getBoardSuccess( data))
        } catch (error) {
            console.log(error)
            dispatch(getBoardFailure())
        }
        try {
            emit(gameId)
        } catch (error) {
            console.log(error)
            try {
                const uri = buildWsUri(gameId, "reconected")
                initWebSocket(dispatch, uri)
                emit(gameId)
                dispatch(connected(gameId))
            } catch (error) {
                console.log(error)
            }
        }
    }
}