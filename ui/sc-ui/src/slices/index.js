import { combineReducers } from 'redux'

import boardReducer from './board'
import secretReducer from './secretboard'
import homeReducer from './home'


const rootReducer = combineReducers({
    board: boardReducer,
    secretboard: secretReducer,
    home: homeReducer
})

export default rootReducer