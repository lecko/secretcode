import { createSlice } from '@reduxjs/toolkit'
import {buildWsUri, initWebSocket} from "../websocket/websocket";

export const initialState = {
    loading: false,
    hasErrors: false,
    board: [],
    gameId: "",
    connected: ""
}

const boardSlice = createSlice({
    name: 'secretboard',
    initialState,
    reducers: {
        getBoard :(state, { payload } ) => {
            //state.loading = true
            state.gameId = payload
        },
        getBoardSuccess: (state,  { payload } ) => {
            state.board = payload
            state.loading = false
            state.hasErrors = false
        },
        getBoardFailure: state => {
            state.loading = false
            state.hasErrors = true
        },
        connected :(state, { payload } ) => {
            state.connected = payload
        },
    },
})

export const {
    getBoard,
    getBoardSuccess,
    getBoardFailure,
    connected
} = boardSlice.actions
export const boardSelector = state => state.secretboard
export default boardSlice.reducer

export function fetchBoard(gameId) {
    return async dispatch => {
        dispatch(getBoard(gameId))

        try {
            const response = await fetch(
                `/secretcode/services/secretcode/game/${gameId}/secret`
            )
            const data = await response.json()
            dispatch(getBoardSuccess(data))

        } catch (error) {
            console.log(error)
            dispatch(getBoardFailure())
        }
        console.log("LECKO" + boardSelector.connected + " " + gameId)
        if (boardSelector.connected !== gameId) {
            try {
                const uri = buildWsUri(gameId, "test")
                initWebSocket(dispatch, uri)
                dispatch(connected(gameId))
            } catch (error) {
                console.log(error)
            }
        }
    }
}


export function fetchBoardWs(gameId) {
    return async dispatch => {

        try {
            const response = await fetch(
                `/secretcode/services/secretcode/game/${gameId}/secret`
            )
            const data = await response.json()
            dispatch(getBoardSuccess(data))

        } catch (error) {
            console.log(error)
            // dispatch(getBoardFailure())
        }
    }
}