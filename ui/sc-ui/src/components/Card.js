import React from 'react'

export const Card = ({ card }) => (
    <aside className="comment">
        <h2>{card.first}</h2>
        <h3>{card.second}</h3>
        <h3>{card.third}</h3>
    </aside>
)
