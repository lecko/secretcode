import React from 'react';

export default class If extends React.PureComponent {
    render() {
        return this.props.condition ? React.Children.only(this.props.children) : null;
    }
}