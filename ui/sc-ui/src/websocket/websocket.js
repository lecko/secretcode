import {fetchBoardWs} from "../slices/board";
import {fetchBoardWs as fetchSecretBoardWs} from "../slices/secretboard";

const W3CWebSocket = require('websocket').w3cwebsocket;

let client = null

const initWebSocket = (dispatch, uri) => {
    // add listeners to socket messages so we can re-dispatch them as actions
    client = new W3CWebSocket(uri);

    client.onerror = function() {
        console.log('Connection Error');
    };

    client.onopen = function() {
        console.log('WebSocket Client Connected');
    };

    client.onclose = function() {
        console.log('echo-protocol Client Closed');
    };

    client.onmessage = function(e) {
        if (typeof e.data === 'string') {
            dispatch(fetchBoardWs(e.data))
            dispatch(fetchSecretBoardWs(e.data))
        }
    };
}

const emit = (gameId) => {
    if (client) {
        client.send(gameId);
    }
}

const buildWsUri = (game, user) => {

    let host = 'mitresarroyos.com/secretcode/services'
    // if (window.location.hostname.indexOf('localhost') >= 0 ) {
    //     host = 'mitresarroyos.com/secretcode/services'
    //     //this.url = 'http://45.33.75.37/services/'
    // }

    return `ws://${host}/ws/${game}/${user}`
}

export {
    initWebSocket,
    emit,
    buildWsUri
}
