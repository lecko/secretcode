import React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
} from 'react-router-dom'

import { Navbar } from './components/Navbar'
import GameBoardPage from "./pages/GameBoard";
import 'bootstrap/dist/css/bootstrap.min.css';
import SecretBoardPage from "./pages/SecretBoard";
import Home from "./pages/Home";

const App = () => {
    return (
        <Router basename='/secretcode'>
            <Navbar />
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/game/:id" component={GameBoardPage} />
                <Route exact path="/game/:id/secret" component={SecretBoardPage} />
                <Redirect to="/" />
            </Switch>
        </Router>
    )
}

export default App
