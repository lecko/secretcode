import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { Button, Badge} from 'react-bootstrap';
import { fetchBoard, boardSelector, showCard } from '../slices/board'

import If from "../components/If";
import Table from "react-bootstrap/Table";

const GameBoardPage = ({ match }) => {
    const dispatch = useDispatch()
    const {
        gameId: gameId,
        board: board,
        loading: boardLoading,
        hasErrors: boardHasErrors,
    } = useSelector(boardSelector)

    useEffect(() => {
        const { id } = match.params

        dispatch(fetchBoard(id))
    }, [dispatch, match])

    const buildCell = (card) => {

        if (card === undefined) return <div></div>

        return <div>
                <If condition={card.third === undefined}>
                    <div class="card">
                        <b class="card_word" > <Button class="" variant="ligth" onClick = {() => {
                            //const { dispatch } = this.props;
                            dispatch(showCard(gameId, card.first));
                        }}>{card.second.toUpperCase()}</Button> {' '} </b>
                    </div>
                </If>

                <If condition={card.third == "BLUE"}>
                    <div class="card blue_spy"></div>
                </If>
                <If condition={card.third == "RED"}>
                    <div class="card red_spy"></div>

                </If>
                <If condition={card.third == "YELLOW"}>
                    <div class="card skip_card"></div>
                </If>
                <If condition={card.third == "BLACK"}>
                    <div class="card killer_card"></div>
                </If>
            </div>

    }

    const renderBoard = () => {
        if (boardLoading) return <p>Loading board...</p>
        if (boardHasErrors) return <p>Unable to display board.</p>
        if (board.size === 0) return <div></div>
        return <Table striped bordered hover>
            <tbody>
                <tr>
                    <td>{buildCell(board[0])}</td>
                    <td>{buildCell(board[1])}</td>
                    <td>{buildCell(board[2])}</td>
                    <td>{buildCell(board[3])}</td>
                    <td>{buildCell(board[4])}</td>
                </tr>
                <tr>
                    <td>{buildCell(board[5])}</td>
                    <td>{buildCell(board[6])}</td>
                    <td>{buildCell(board[7])}</td>
                    <td>{buildCell(board[8])}</td>
                    <td>{buildCell(board[9])}</td>
                </tr>
                <tr>
                    <td>{buildCell(board[10])}</td>
                    <td>{buildCell(board[11])}</td>
                    <td>{buildCell(board[12])}</td>
                    <td>{buildCell(board[13])}</td>
                    <td>{buildCell(board[14])}</td>
                </tr>
                <tr>
                    <td>{buildCell(board[15])}</td>
                    <td>{buildCell(board[16])}</td>
                    <td>{buildCell(board[17])}</td>
                    <td>{buildCell(board[18])}</td>
                    <td>{buildCell(board[19])}</td>
                </tr>
                <tr>
                    <td>{buildCell(board[20])}</td>
                    <td>{buildCell(board[21])}</td>
                    <td>{buildCell(board[22])}</td>
                    <td>{buildCell(board[23])}</td>
                    <td>{buildCell(board[24])}</td>
                </tr>
            </tbody>
        </Table>


    }

    return (
        <section>
            <h2>Juego: {gameId}</h2>
            {renderBoard()}
        </section>
    )
}

export default GameBoardPage
