import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { Button, Badge} from 'react-bootstrap';
import { createGame, homeSelector, setGame} from '../slices/home'


import FormGroup from "react-bootstrap/FormGroup";
import FormControl from "react-bootstrap/FormControl";
import {Link} from "react-router-dom";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";

const Home = ({ match }) => {
    const dispatch = useDispatch()
    const {
        gameId: gameId
    } = useSelector(homeSelector)


    return (
        <section>
            <h2>Código secreto <h5>¿No sabes como jugar? <a target="_blank" href="http://devir.es/wp-content/uploads/2015/12/Codigo-Secreto-reglas.pdf"> Manual</a> o <a target="_blank" href="https://www.youtube.com/watch?v=lMf7MnQsSNg"> Video</a></h5>
            </h2>
            <h5>Hace Click en <i><b>Nuevo Juego</b></i> y ¡pasale el nro a tu amigo remoto en cuarentena!</h5>
            <br/>
            <h5>Despues elegi si queres dar pistas(<i><b>Secret Board</b></i>) o adivinar (<i><b>Board</b></i>)</h5>
            <br/>
            <h5>¡Siempre empieza el equipo <b>Rojo</b>!</h5>
            <Button variant="primary" onClick = {() => {
                //const { dispatch } = this.props;
                dispatch(createGame());
            }}>Nuevo Juego</Button>
            
            <br/>

            <FormGroup
                controlId="formBasicText"
            >
                <FormControl
                    type="text"
                    value={gameId}
                    placeholder="Nro de Juego"
                    onChange={(e) => {dispatch(setGame(e.target.value))}}
                />
                <FormControl.Feedback />
                {/*<HelpBlock>Validation is based on string length.</HelpBlock>*/}
            </FormGroup>
            <Link to={`/game/${gameId}`} className="button">
                Adivinar (Board)
            </Link> { ' '}
            <Link to={`/game/${gameId}/secret`} className="button">
                Dar Pistas (Secret Board)
            </Link>

        </section>
    )
}

export default Home
